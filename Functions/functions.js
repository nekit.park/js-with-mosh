// Function declaration 
function walk() {
  console.log('walk')
}

// Function expression 
let run = function() {
  console.log('run');
};

run();
walk()

// Arguments
function sum(){
  let result = 0;
  console.log(arguments); // Arguments это псевдомассив или коллекция со всеми аргументами переданными в функцию, свойством length, но без методов массива.
  for (let number of arguments)
    result += number;
  return result;
};

console.log(sum(12, 5, 55));
// Rest operator 

function sum2(discount, ...rest){ //Перед REST оператором может быть сколько угодно аргументов, но он должен быть всегда последним аргументом функции.
  console.log(rest) // Rest оператор это полноценный массив со всеми методами массива, который собирает в себя все аргументы функции.
  let total = rest.reduce( (a, b) => a + b );
  return total * (1 - discount);
}
console.log(sum2(0.1, 5, 10, 15));

// Getters and Setters

const person = {
  firstName: 'Nikita',
  lastName: 'Park',
  get fullName() {
    return `${this.firstName} ${this.lastName}`
  },
  set fullName(value) { // Через функцию сеттер мы можем задавать новые значения свойствам присваивая их извне обьекта
    if (typeof value !== 'string') // Сначала создаем условие при котором может возникнуть ошибка.
      throw new Error('Value is not a string.');// Затем создаем новую ошибку с сообщением.

    let split = value.split(' ');
    if (split.length < 2) 
      throw new Error('Enter name and last name')
    this.firstName = split[0];
    this.lastName = split[1];
  }
}
// try {
//   person.fullName = "Luck";
// }
// catch(e) {
//   console.log(e);
// }
person.fullName = 'Casey Naistat';
console.log(person.fullName);
console.log(person);

// This !
const video = {
  title: 'a',
  tags: ['a', 'b', 'c'],
  showTags() {
    this.tags.forEach(tag => console.log(this.title, tag));
  }
}

video.showTags();
