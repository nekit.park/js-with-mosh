// Task 1
function sum(...rest){
  if (rest.length === 1 && Array.isArray(rest[0]))
    rest = [...rest[0]];

  return rest.reduce( (a, b) => a + b );
}

console.log(sum([10,5,9]));

// Task 2
const circle = {
  radius: 1,
  get area(){ // Мы не спожем извне обьекта задать area новое значение потому что у него нет set сеттера.
    return Math.PI * this.radius * this.radius;
  }
};

console.log(circle.area);
circle.radius = 5;
console.log(Math.round(circle.area));

// Task 3
try {
  const numbers = [1, 2, 3, 4, 1];
  const count = countOccurrences(numbers, 1);
  console.log(count);
}
catch (e) {
  console.log(e.message);
}

function countOccurrences( array, searchElem ){
  if (!Array.isArray(array))
   throw new Error('Need to use array');

  return array.reduce( (accum, current) => {
    let occur = (current === searchElem) ? 1 : 0;
  return accum + occur;
  }, 0)
}


