// Add elements into array
const array = [3, 4];

// End  of array 
array.push(5, 6);

// Start
array.unshift(1, 2);

// Middle
array.splice(2, 0, 'a', 'b', 'c');
console.log(array);

// Finding elements 
const numbers = [1, 2, 3, 4, 5, 3];

console.log(numbers.indexOf(3));
console.log(numbers.lastIndexOf(3));
console.log(numbers.includes(8));

// Finding reference type elements
const courses = [
  { id: 5, name: 'a' },
  { id: 1, name: 'b'}
];

// let course = courses.find(function(element){
//   return element.name === 'b';
// });

// arrow function
let course = courses.find(element => element.name === 'b');
console.log(course);

let courseIndex = courses.findIndex(function(element){
  return element.name === 'a';
})
console.log(courseIndex);

// Removing elements 
const newArray = [1, 2, 3, 4, 5];

// From End
// let last = newArray.pop();
// console.log(last);
// console.log(newArray);

// From Start
// let first = newArray.shift();
// console.log(first);
// console.log(newArray);

newArray.splice(1, 2);
console.log(newArray);

// Combining and slicing arrays

const first = [1, 2, 3];
const second = [4, 5, 6];

const combine = first.concat(second);
console.log(combine);

const slice = combine.slice(0, 4);
console.log(slice);

// Spread operator
const arr1 = [1, 2, 3];
const arr2 = [4, 5, 6];

const combined = [...arr1, 'a', ...arr2, 'b'];
console.log(combined);

// Iterating an array
// for (let number of numbers){
//   console.log(number)
// };

numbers.forEach((index, number) => console.log(index, number));

// Joining arrays

const words = ['My', 'message', 'to', 'you'];
const wordsStr = words.join(' ');
console.log(wordsStr);

// Sorting arrays 
const numbArr = [2, 1, 6, 5];
numbArr.sort();
console.log(numbArr);

const newCourses = [
  { id: 1, name: 'Node.js' },
  { id: 2, name: 'Java Script' }
];

newCourses.sort((a, b) => {
  let aCourse = a.name.toUpperCase();
  let bCourse = b.name.toUpperCase();
  if (aCourse < bCourse) return -1;
  if (aCourse > bCourse) return 1;
  return 0;
})

console.log(newCourses);

// Testing the elements of an array
const someNumb = [1, 4, -5, 9];

let allPositive = someNumb.every(number => {return number >= 0});
console.log(allPositive);

let alLeastOneNegative = someNumb.some(number => {return number < 0});
console.log(alLeastOneNegative);

// Filtering an Array
const positiveArray = someNumb.filter(number => number > 0);
console.log(positiveArray);

//Reducing an Array
const sum = someNumb.reduce(
  (accum, currentVal) => accum + currentVal
  );

console.log(sum);