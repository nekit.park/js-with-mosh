function CreateNewUser() {
  let firstName = prompt('Enter your first name');
  let lastName = prompt('Enter your last name');
  this.firstName = firstName;
  this.lastName = lastName;
  this.getLogin = () => {
    let firstLetter = this.firstName.split('', 1).join().toLowerCase();
    let login = firstLetter + '.' + this.lastName.toLowerCase();
    console.log(login);
  }
};

// let user = new CreateNewUser();
// console.log(user);

// Task 7

const firstArr = [
  { id: 1, name: 'Bob' },
  { id: 2, name: 'Luck' },
  { id: 3, name: 'Nik' },
];

const secondArr = [
  { id: 1, name: 'Kook' },
  { id: 1, name: 'Bob' },
  { id: 1, name: 'Ben' }
];

function excludeBy(arr1, arr2, name){

}

// Mosh task 1

const n = arrayFromRange(1, 10);
console.log(n);

function arrayFromRange(min, max) {
  const array = [];
  for (min; min <= max; min++)
    array.push(min);
  return array;
}

// Task 2
const numbersArr = [1, 2, 3, 4];
console.log(includes(numbersArr, -3));


function includes(array, searchElem){
  for (let item of array)
    if (item === searchElem)
      return true;
    return false;
};

// Task 3
console.log(numbersArr);
const result = except(numbersArr, [1, 2]);
console.log(result);

function except(arr, excluded){
 const output = [];
 for (let item of arr){
   if(!excluded.includes(item))
    output.push(item)
 }
 return output;
}

// Task 4
const newNumbers = [1, 2, 3, 4, 5, 6, 7, 1, 10];
const output = move(newNumbers, 5, -3);
console.log(output);

function move(array, index, offset){
  const position = index + offset;
  if (position >= array.length || position < 0) {
    console.error('Invalid offset');
    return;
  }
  const output = [...array];
  const num = output.splice(index, 1)[0];
  output.splice(position, 0, num);
  return output;
}

//Task 5
const count = countOccurrences(newNumbers, 1);
console.log(count);

function countOccurrences(array, searchElem){
 let count = 0;

 array.forEach(item => {
  if (item === searchElem)
  count++
 })

 return count;
}

// Task 6
const max = getMax(newNumbers);
console.log(max);

function getMax(array){
  let max = 0;
  array.forEach(item => {
    if (item > max) max = item;
  });
  return max;
}

// Task 7
// const movies = [
//   { title: 'a', year: 2018, rating: 4.5 },
//   { title: 'b', year: 2018, rating: 4.7 },
//   { title: 'c', year: 2018, rating: 3 },
//   { title: 'd', year: 2017, rating: 4.5 }
// ];

// const newMovies = movies.filter(elem => {
//   return (elem.year === 2018 && elem.rating >= 4);
// });
// console.log(newMovies);

// const rating = movies
//   .sort((a, b) => a.rating - b.rating)
//   .reverse()
//   .map(a => a.title);

// console.log(rating);

// Task 8 
const movies = [
  { title: 'a', year: 2017, rating: 4.7 },
  { title: 'b', year: 2018, rating: 4.5 },
  { title: 'c', year: 2018, rating: 3 },
  { title: 'd', year: 2015, rating: 4.2 }
];

const excludeMovies = [
  { title: 'a', year: 2017, rating: 4.7 },
  { title: 'b', year: 2018, rating: 4.5 },
  { title: 'f', year: 2018, rating: 3 },
  { title: 'l', year: 2015, rating: 4.2 }
];

function excludeBy(arr1, arr2, title){
  return arr1.filter(obj => {
    for (let obj2 of arr2) 
      if (obj[title] === obj2[title]) return false;
    return true;
  });
}

const output12 = excludeBy(movies, excludeMovies, 'title');
console.log(output12);