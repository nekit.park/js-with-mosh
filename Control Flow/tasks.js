// function getMaxNumber ////////// 1 /////////////

// function getMaxNumber(a, b) {
//   return (a > b) ? a : b;
// }
// let result = getMaxNumber(14, 35);
// console.log(result);
///////////////  2  /////////////////
// function isLandscape(width, height) {
//   return (width > height);
// }
// let result = isLandscape(100, 200);
// console.log(result);
//////////////   3   ////////////////
// function fizzBuzz(input) {
//   if (typeof input !== 'number')
//     return NaN;
//   if ((input % 3 === 0) && (input % 5 === 0))
//     return 'FizzBuzz';
//   if (input % 3 === 0)
//     return 'Fizz';
//   if (input % 5 === 0)
//     return 'Buzz';
//   return input;
// }

// console.log(fizzBuzz(2));
/////////////////////   4  ///////////////////////
// function checkSpeed(speed) {
//   const speedLimit = 70;
//   const speedPointKm = 5;

//   if (speed < speedLimit + speedPointKm) {
//     console.log('Ok');
//     return;
//   }
//   const point = Math.floor((speed - speedLimit) / 5);
//     if (point >= 12)
//       console.log('License suspended');
//     else
//       console.log('You have ', point, ' points');
// }

// checkSpeed(140);

//////////////////   5   /////////////////

// function showNumbers(limit) {
//   for (let i = 0; i <= limit; i++) {
//     if (i % 2 === 0) console.log(`${i} - Even number`);
//     else console.log(`${i} - Odd number`);
//   }
// }

// showNumbers(25);

//////////////    6    /////////////////

// function truthyCount(arr) {
//   let count = 0;
//   for (let key of arr)
//     if (key) 
//       count++;
//   return count;
// }

// const array = [0, 1, null, 2, 3, undefined, NaN, 4, 5, ''];

// console.log(truthyCount(array));


////////////////////    7    /////////////////

// const movie = {
//   name: 'Harry Potter',
//   releaseYear: 2008,
//   rating: 8.9,
//   director: 'Rolling'
// }

// function showProperties(obj) {
//   for (let key in obj) {
//     if (typeof obj[key] === 'string')
//       console.log(key, obj[key]);
//   }
// }

// showProperties(movie);

/////////////////    8      ////////////////

// function sum(limit) {
//   let finalSum = 0;
//   for (let i = 3; i <= limit; i++)
//     if (i % 3 === 0 || i % 5 === 0)
//       finalSum += i;

//   return finalSum;
// }

// console.log(sum(100));

//////////////   9   //////////////////

// const studentGrade = [96, 92, 96];

// function calculateGrade(grade) {
//   let average = 0;
//   for (let index of grade)
//     average += index;
//   average /= grade.length;
//   if (average < 60)
//     console.log('F');
//   else if (average > 59 && average <= 69)
//     console.log('D');
//   else if (average > 69 && average <= 79)
//     console.log('C');
//   else if (average > 79 && average <= 89)
//     console.log('B');
//   else console.log('A')
// }

// calculateGrade(studentGrade);

//////////////////////////////////////////    10     /////////////////////////////////////////

function showStars(rows) {
  for (let row = 1; row <= rows; row++){
    let stars = '';
    
    for (let i = 0; i < row; i++)
      stars += '*';
      console.log(stars);
    }
}

showStars(8);