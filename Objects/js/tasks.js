const address = {
  street: 'a',
  city: 'b',
  zipCode: 123
}

function showAddress(obj){
  for (let key in obj){
    console.log(`The key is: ${key}, The property is: ${obj[key]}`);
  }
}
showAddress(address);

// 2 task
// function constructor
function NewAddress(street, city, code) {
  this.street = street;
  this.city = city;
  this.zipCode = code;
}

const street1 = new NewAddress('w', 'r', 1215);
const street2 = new NewAddress('y', 'r', 1215);
console.log(street1);
console.log(street2);

// factory function 
function createAddress(street, city, code) {
  return {
    street,
    city,
    code
  }
}

const street3 = createAddress('z', 'x', 999);
console.log(street3);

// Task 3
function areEqual(street1, street2){
  return street1.street === street2.street && street1.city === street2.city && street1.zipCode === street2.zipCode;
}

function areSame(street1, street2){
  return street1 === street2;
}

// Task 4
const blog = {
  title: 'Nik',
  body: 'a',
  author: 'Casey',
  views: 125,
  comments: {
    author: 'John',
    body: 'b'
  },
  isLive() {
    return true
  }
}

console.log(blog);

// Task 5
function Post(title, body, author){
  this.title = title;
  this.body = body;
  this.author = author;
  this.views = 0;
  this.comments = [];
  this.isLive = false;
}
const post = new Post('a', 'v', 'n');
console.log(post);

