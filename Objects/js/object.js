// Factory function
function createCircle (radius) {
  return {
    radius,
    draw() {
        console.log('circle');
    }
  }
};

const circle1 = createCircle(12);
console.log(circle1);

//Constructor function
function Circle(radius){
  this.radius = radius;
  this.draw = function(){
    console.log('object');
  }
}

const circle = new Circle(21);
console.log(circle);

// properties of an object
let object = {
  color: 'blue',
  size: 12,
  method() {
    console.log('test');
  }
}

for (let key in object) {
  console.log(key, object[key]);
}

for (let k of Object.keys(object)){
  console.log(k);
}

for (let entry of Object.entries(object)){
  console.log(entry);
}

// cloning objects

// let newObj = {};
// for (let key in object) {
//   newObj[key] = object[key]
// }

// const newObj = Object.assign( {}, object )

const newObj = { ...object };

console.log(newObj);