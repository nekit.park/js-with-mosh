function User(name, age, email) {
  this.name = name;
  this.age = age;
  this.email = email;
  this.online = false;
};

User.prototype.login = function() {
  this.online = true;
  console.log(`${this.email} already log in`);
}

User.prototype.logout = function() {
  this.online = false;
  console.log(`${this.email} already log out`);
}
function Admin(admin, ...rest) {
  User.apply(this, rest);
  this.admin = admin;
}

Admin.prototype = Object.create(User.prototype);

Admin.prototype.isAdmin = function() {
  console.log('Only admins method');
};

const admin1 = new Admin('yes', 'Sid', 19, 'sid@mail.com');
admin1.login();
admin1.isAdmin();

const userOne = new User('Alex', 23, 'alex@mail.ru');
const userTwo = new User('Michel', 21, 'michel@mail.ru');
let usersArr = [admin1, userOne, userTwo];
console.log(usersArr);

Admin.prototype.deleteUser = function(user) {
  usersArr = usersArr.filter( u => {
    return u.email !== user.email;
  })
};

admin1.deleteUser(userTwo);
console.log(usersArr)

console.log(admin1)
