class User {
  constructor(userName, userAge, userEmail) {
    this.userName = userName;
    this.userAge = userAge;
    this.userEmail = userEmail;
    this.score = 0;
    this.level = 0;
  }
  login() {
    console.log(`${this.userEmail} already logged in`);
    return this;
  }
  logout() {
    console.log(`${this.userEmail} already logged out`);
    return this;
  }
  plusScore() {
    this.score++;
    if (this.score === 3) {
      console.log(`${this.userName} congrats your score is ${this.score} and you update your level`)
      this.nextLevel();
    } else {
      console.log(`${this.userName} your level is ${this.level} your score is ${this.score}`);
    }
    
    return this;
  }
  nextLevel() {
      this.level++;
      this.score = 0;
      console.log(`${this.userName} your level is ${this.level} your score is ${this.score}`)
  }
};

class Admin extends User {
  deleteUser(user) {
    users = users.filter( u => {
      return u.userEmail != user.userEmail;
    })
  }
}

const user1 = new User('Nikita', 24, 'nik@gmail.com');
const user2 = new User('Casey', 38, 'casey@mail.ru');
user1
.login()
.plusScore()
.plusScore()
.plusScore()
.logout()

const admin = new Admin('Alex', 27, 'admin@mail.ru');
console.log(admin);

let users = [user1, user2, admin];
console.log(users);

admin.deleteUser(user2);
console.log(users)
