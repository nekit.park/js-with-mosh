const object = {
  radius: 1,
  location: {
    x: 2,
    y: 2
  }
};

console.log(object);
delete object.location;
object.newLocation = { x: 1, y: 1 };
console.log(object);

object.showMessage = () => console.log('Test');
object.showMessage();
console.log(object);

for (key in object)
  if ( typeof object[key] !== 'function')
  console.log(key, object[key]);

const keys = Object.keys(object);
console.log(keys);

// 5.05

function Circle(radius) {
  this.radius = radius;
  let defaultLoc = { x: 0, y: 0 };
  Object.defineProperty(this, 'defaultLoc', {
    get: () => { // с помощью геттера get мы можем разрешить на чтение наши скрытые свойства
      return defaultLoc
    },
    set: (newVal) => defaultLoc = newVal // с помощью сеттера set мы можем разрешить перезаписывать свойства путем присваивания 
  })
};


const newCircle = new Circle(12);
console.log(newCircle);
console.log(newCircle.defaultLoc); // Чтение
newCircle.defaultLoc = { x: 10, y: 10 }; // Перезапись присваиванием
console.log(newCircle.defaultLoc);





