// Stop watch
function StopWatch() {
  let startTime, endTime, running, duration = 0;

  this.start = () => {
    if (running) 
      throw new Error ('The start time already start');

    running = true;

    startTime = new Date();
  };

  this.stop = () => {
    if (!running) 
      throw new Error ('The start time is not started');
    
    running = false;

    endTime = new Date();

    const seconds = (endTime.getTime() - startTime.getTime()) / 1000;
    duration += seconds;
  };

  this.reset = () => {
    startTime = null;
    stopTime = null;
    running = false;
    duration = 0;
  };

  Object.defineProperty(this, 'duration', {
    get: () => duration
  })
};

const watch = new StopWatch();

