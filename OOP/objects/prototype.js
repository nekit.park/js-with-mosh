const user = {
  name: 'Nikita',
  lastName: 'Park'
};

Object.defineProperty(user, 'name', {
  writable: false, // cant change
  enumerable: false, // cant loop
  configurable: false // Cant delete
});

user.name = 'Luck';
console.log(user.name)

for ( let key in user )
  console.log(key);

// Prototype
function Circle(radius) {
  this.radius = radius;
  this.sayHi = () => console.log('Hi');
};

Circle.prototype.draw = () => console.log('This method added into prototype');
// Added new method into prototype of Circle constructor function
const circle = new Circle(12);
circle.draw();
console.log(circle);

// Return only own members
console.log(Object.keys(circle));

// Return all members (own and prototype)
for (let key in circle) console.log(key);

