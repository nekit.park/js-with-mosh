const student1 = {
  'first name': 'Nerest',
  'last name': 'Becker',
  tabel: {
    history: 12,
    biology: 9,
    physicks: 8
  },
  calcAverageScore: function() {
    let itemsSum = 0;
    for (let key in this.tabel) {
      console.log(this.tabel[key])
      itemsSum += this.tabel[key];
    };
    return itemsSum / Object.keys(this.tabel).length;
  },
  getLowestScore: function() {
    let lowest = 12;
    for (let key in this.tabel) {
      if (lowest > this.tabel[key]) lowest = this.tabel[key];
    }
    return lowest;
  }
};
const student2 = {
  'first name': 'Vika',
  'last name': 'Odincova',
  tabel: {
    history: 9,
    biology: 7,
    physicks: 8
  },
  calcAverageScore: function() {
    let itemsSum = 0;
    for (let key in this.tabel) {
      console.log(this.tabel[key])
      itemsSum += this.tabel[key];
    };
    return itemsSum / Object.keys(this.tabel).length;
  },
  getLowestScore: function() {
    let lowest = 12;
    for (let key in this.tabel) {
      if (lowest > this.tabel[key]) lowest = this.tabel[key];
    }
    return lowest;
  }

};
const student3 = {
  'first name': 'Sasha',
  'last name': 'Grey',
  tabel: {
    history: 7,
    biology: 12,
    physicks: 12
  },
  calcAverageScore: function() {
    let itemsSum = 0;
    for (let key in this.tabel) {
      console.log(this.tabel[key])
      itemsSum += this.tabel[key];
    };
    return itemsSum / Object.keys(this.tabel).length;
  },
  getLowestScore: function() {
    let lowest = 12;
    for (let key in this.tabel) {
      if (lowest > this.tabel[key]) lowest = this.tabel[key];
    }
    return lowest;
  }

};

